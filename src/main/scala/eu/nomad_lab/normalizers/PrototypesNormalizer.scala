/*
 * Copyright 2017-2018 Daria M. Tomecka, Fawzi Mohamed
 * 
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package eu.nomad_lab.normalizers

import eu.{ nomad_lab => lab }
import eu.nomad_lab.DefaultPythonInterpreter
import org.{ json4s => jn }
import scala.collection.breakOut
import eu.nomad_lab.normalize.ExternalNormalizerGenerator
import eu.nomad_lab.meta
import eu.nomad_lab.query
import eu.nomad_lab.normalize.Normalizer
import eu.nomad_lab.resolve._
import eu.nomad_lab.ref.ObjectKind
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.h5.H5EagerScanner
import eu.nomad_lab.h5.SectionH5
import eu.nomad_lab.parsers.ExternalParserWrapper
import eu.nomad_lab.JsonSupport
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.JsonUtils
import scala.collection.mutable.StringBuilder

object PrototypesNormalizer extends ExternalNormalizerGenerator(
  name = "PrototypesNormalizer",
  info = jn.JObject(
    ("name" -> jn.JString("PrototypesNormalizer")) ::
      ("parserId" -> jn.JString("PrototypesNormalizer" + lab.PrototypesVersionInfo.version)) ::
      ("versionInfo" -> jn.JObject(
        ("nomadCoreVersion" -> jn.JObject(lab.NomadCoreVersionInfo.toMap.map {
          case (k, v) => k -> jn.JString(v.toString)
        }(breakOut): List[(String, jn.JString)])) ::
          (lab.PrototypesVersionInfo.toMap.map {
            case (key, value) =>
              (key -> jn.JString(value.toString))
          }(breakOut): List[(String, jn.JString)])
      )) :: Nil
  ),
  context = "calculation_context",
  filter = query.CompiledQuery(query.QueryExpression("wyckoff_letters_primitive"), meta.KnownMetaInfoEnvs.publicMeta),
  cmd = Seq(DefaultPythonInterpreter.pythonExe(), "${envDir}/normalizers/prototypes/normalizer/normalizer-prototypes/classify4me_prototypes.py",
    "${contextUri}", "${archivePath}"),
  resList = Seq(
    "normalizer-prototypes/setup_paths.py",
    "normalizer-prototypes/classify4me_prototypes.py",
    "nomad_meta_info/public.nomadmetainfo.json",
    "nomad_meta_info/common.nomadmetainfo.json",
    "nomad_meta_info/meta_types.nomadmetainfo.json",
    "nomad_meta_info/stats.nomadmetainfo.json"
  ) ++ DefaultPythonInterpreter.commonFiles(),
  dirMap = Map(
    "normalizer-prototypes" -> "normalizers/prototypes/normalizer/normalizer-prototypes",
    "nomad_meta_info" -> "nomad-meta-info/meta_info/nomad_meta_info",
    "python" -> "python-common/common/python/nomadcore"
  ) ++ DefaultPythonInterpreter.commonDirMapping(),
  metaInfoEnv = lab.meta.KnownMetaInfoEnvs.all
) {

  case class PrototypeInfo(
    section_system_uri: String,
    wyckoff_letters_primitive: Seq[String],
    atomic_numbers_primitive: Seq[Int],
    space_group_number: Int
  )

  override def stdInHandler(context: ResolvedRef)(wrapper: ExternalParserWrapper)(pIn: java.io.OutputStream): Unit = {
    val out: java.io.Writer = new java.io.BufferedWriter(new java.io.OutputStreamWriter(pIn));
    val trace: Boolean = Normalizer.trace
    val stringBuilder = if (trace)
      new StringBuilder
    else
      null
    def writeOut(s: String): Unit = {
      out.write(s)
      if (trace) stringBuilder ++= s
    }
    def flush(): Unit = {
      out.flush()
      if (trace) {
        logger.info(stringBuilder.result())
        stringBuilder.clear()
      }
    }
    writeOut("[")
    var isFirst = true
    try {
      context match {
        case Calculation(archiveSet, c) =>
          val sysTable = c.sectionTable(Seq("section_run", "section_system"))
          val symmetryTable = sysTable.subSectionTable("section_symmetry")
          val stdSysTable = symmetryTable.subSectionTable("section_primitive_system")
          var lastSys: Long = -1L
          for (stdSys <- stdSysTable) {
            val symm: SectionH5 = stdSys.parentSection.get
            val symmMethod: Option[String] = symm.maybeValue("symmetry_method").map(_.stringValue)
            symmMethod match {
              case Some("spg_normalized") =>
                val sysSection = symm.parentSection.get
                if (sysSection.gIndex != lastSys) {
                  lastSys = sysSection.gIndex
                  for (atomNrs <- stdSys.maybeValue("atomic_numbers_primitive").map(_.seqLongValue())) {
                    for (wyckoffs <- stdSys.maybeValue("wyckoff_letters_primitive").map(_.seqStringValue())) {
                      for (spacegroup <- symm.maybeValue("space_group_number").map(_.intValue)) {
                        val pInfo = PrototypeInfo(
                          section_system_uri = sysSection.toRef.toUriStr(ObjectKind.NormalizedData),
                          wyckoff_letters_primitive = wyckoffs,
                          atomic_numbers_primitive = atomNrs.map(_.intValue),
                          space_group_number = spacegroup
                        )
                        if (!isFirst)
                          writeOut(",\n")
                        else
                          isFirst = false
                        writeOut(JsonSupport.writeNormalizedStr(pInfo))
                        flush()
                      }
                    }
                  }
                }
              case _ => ()
            }
          }
          writeOut("]")
          flush()
        case r =>
          throw new Exception(s"FhiAimsBasisNormalizer expected a calculation as context, but got $r")
      }
    } finally {
      out.close()
      pIn.close()
      wrapper.sendStatus = ExternalParserWrapper.SendStatus.Finished
    }
  }
}
