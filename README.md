Normalizer for the data classification on the basis of known prototypes.

The original repository lives at:
https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-prototypes

More info on structure types can be found here:

https://gitlab.mpcdf.mpg.de/nomad-lab/encyclopedia-general/wikis/Structure-type

The classification "algorithm" is described here:

https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-prototypes/wikis/info-on-classification-by-prototypes

More information on the normalization of classifiaction by prototypes one may find here:
https://gitlab.mpcdf.mpg.de/nomad-lab/normalizer-prototypes/wikis/normalization-of-classification-by-prototypes

All the prototypes as included in `structure_types.py` are updated in their main location from where one can import them:

`/nomad-lab-base/python-common/common/python/nomadcore/structure_types.py`

Location on gitlab:

https://gitlab.mpcdf.mpg.de/nomad-lab/python-common/blob/master/common/python/nomadcore/structure_types.py